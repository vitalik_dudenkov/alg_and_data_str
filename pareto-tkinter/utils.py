import tkinter as tk
import random as rnd
from init import *
from tkinter import filedialog as fd
from tkinter import messagebox as mb


def print_axes(x0, y0, xm, ym, color, canvas):
    '''
        печатает оси координат
    '''
    canvas.create_line(x0, y0, xm, y0, fill=color, arrow=tk.LAST, dash=(4, 2))
    canvas.create_line(x0, y0, x0, ym, fill=color, arrow=tk.LAST, dash=(4, 2))


class Smartphone():
    def __init__(self, id, name, time, power):
        self.id = id
        self.name = name
        self.time = time
        self.power = power

    def isDominate(self, A):
        if A.time >= self.time and A.power >= self.power:
            return True
        return False

def runPareto(canvas):
    # fileName = 'smarts.txt'
    fileName = fd.askopenfilename()
    if fileName == '':
        mb.showerror("Упс!", "Необходимо выбрать файл!")
    else:
        f = open(fileName, "r")
        lines = f.readlines()
        lst = list()
        for i in range (1, len(lines)):
            tmp = lines[i].split(';')
            lst.append(Smartphone(int(tmp[0]), tmp[1], int(tmp[2]), int(tmp[3])))
        buildPareto(lst, canvas)


def buildPareto(lst, canvas):
    flag = True
    for el in lst:
        flag = True
        for dbel in lst:
            if el == dbel:
                continue
            if el.isDominate(dbel):
                flag = False
                # вместо x - time, y - power
                canvas.create_oval(pole + el.time-r, h - pole - el.power-r, pole + el.time+r, h - pole - el.power+r, outline="#009", fill="#f50") 
                break
        if flag:
            canvas.create_oval(pole + el.time-r, h - pole - el.power-r, pole + el.time+r, h - pole - el.power+r, outline="#009", fill="#2f2") 


def print_dots(n, canvas):
    '''
        печатает случайные точки 
        двух цветов в зависимости от координат
    '''
    for _ in range(n):
        x = rnd.randint(pole, w - pole)
        y = rnd.randint(pole, h - pole)
        fill_color = "#f50" if x > y else "#2f2"
        canvas.create_oval(x-r, y-r, x+r, y+r, outline="#009", fill=fill_color)