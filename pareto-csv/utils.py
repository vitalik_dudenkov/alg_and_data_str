import csv
import tkinter as tk
from tkinter import filedialog as fd
from tkinter import messagebox as mb
from init import *

class Smart():
    def __init__(self, name, power, price):
        self.name = name
        self.power = power
        self.price = price
    
    def __repr__(self):
        return '%5d%5d\t%s' % (self.power, self.price, self.name)
    
    def isDominate(self, A, z):
        if z == 1:
            if A.power >= self.power and A.price >= self.price:
                return True
            return False
        elif z == 2:
            if A.power >= self.power and A.price <= self.price:
                return True
            return False
        elif z == 3:
            if A.power <= self.power and A.price >= self.price:
                return True
            return False
        else:
            if A.power <= self.power and A.price <= self.price:
                return True
            return False


def print_axes(x0, y0, xm, ym, color, canvas):
    '''
        печатает оси координат
    '''
    canvas.create_line(x0, y0, xm, y0, fill=color, arrow=tk.LAST, dash=(4, 2))
    canvas.create_line(x0, y0, x0, ym, fill=color, arrow=tk.LAST, dash=(4, 2))

def get_smartphones(name_file):
    lst = []
    with open(name_file) as file:
        reader = csv.DictReader(file, delimiter=';')
        for row in reader:
            try:
                obj = Smart(row['name'], int(row['power']), int(row['price']))
                lst.append(obj)
            except:
                pass
    return lst


def runPareto(canvas, entry):
    fileName = fd.askopenfilename()
    z = entry.get()
    # Тут же получаем уклонение, пока поставлю 1
    # z = 1
    if fileName == '' or z == '':
        mb.showerror("Упс!", "Необходимо выбрать файл и уклонение!")
    else:
        buildPareto(get_smartphones(fileName), canvas, int(z))

def buildPareto(lst, canvas, z):
    flag = True
    for el in lst:
        flag = True
        for dbel in lst:
            if el == dbel:
                continue
            if el.isDominate(dbel, z):
                flag = False
                canvas.create_oval(pole + el.power-r, h - pole - el.price-r, pole + el.power+r, h - pole - el.price+r, outline="#009", fill="#f50") 
                break
        if flag:
            canvas.create_oval(pole + el.power-r, h - pole - el.price-r, pole + el.power+r, h - pole - el.price+r, outline="#009", fill="#2f2") 

