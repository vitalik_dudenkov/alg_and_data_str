import tkinter as tk
from utils import *
from init import *
from tkinter import messagebox as mb


window = tk.Tk()
canvas = tk.Canvas(window, width=w, height=h, bg='#fda')

def openDlg():
    mb.askokcancel(title='Справка', message='На графике:\nОсь абсцисс - мощность\nОсь ординат - цена\nУклонения:\n1 - северо-восточное\n2 - юго-восточное\n3 - северо-западное\n4 - юго-восточное')

btn_start = tk.Button(window,
    text='Выбрать файл и построить',
    width=30,
    bd=2,
    command=lambda: runPareto(canvas, entry)
)

label = tk.Label(
    text='Введите уклонение',
    font='Arial 14',
    bd=2
)

entry = tk.Entry(width=10, bd=2)

btn_info = tk.Button(
    text='Справка',
    width=20,
    command=openDlg
)


label.place(relx=.03, rely=0, bordermode=tk.OUTSIDE)
entry.place(relx=.25, rely=.01, bordermode=tk.OUTSIDE)
btn_start.pack()
btn_info.place(relx=.8, rely=0,)
canvas.pack()
print_axes(x0, y0, xm, ym, 'blue', canvas)
window.mainloop()



