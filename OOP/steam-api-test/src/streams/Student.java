package streams;

public class Student {

    private int id;
    private String lastName;
    private String sex;
    private Integer mathScore;
    private Integer rusScore;
    private Integer infScore;
    private Integer fullScore;
    private int directId;
    
    public Student() {
	
    }
    
    
    public Student(int id, String lastName, String sex, Integer mathScore, Integer rusScore, Integer infScore, int directId) {
	super();
	this.id = id;
	this.lastName = lastName;
	this.sex = sex;
	this.mathScore = mathScore;
	this.rusScore = rusScore;
	this.infScore = infScore;
	this.directId = directId;
    }


    public int getId() {
        return id;
    }
    public String getLastName() {
        return lastName;
    }
    public String getSex() {
        return sex;
    }
    public Integer getMathScore() {
        return mathScore;
    }
    public Integer getRusScore() {
        return rusScore;
    }
    public Integer getInfScore() {
        return infScore;
    }
    public int getDirectId() {
        return directId;
    }
    

    public Integer getFullScore() {
        return fullScore;
    }


    public void setFullScore(Integer fullScore) {
        this.fullScore = fullScore;
    }


    @Override
    public String toString() {
	return "Student [id=" + id + ", lastName=" + lastName + ", sex=" + sex + ", mathScore=" + mathScore
		+ ", rusScore=" + rusScore + ", infScore=" + infScore + ", directId=" + directId + "]";
    }
}
