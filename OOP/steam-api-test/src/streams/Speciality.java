package streams;

public class Speciality {

    private int id;
    private String  name;
    private int minScore;
    private int limit;
    
    public Speciality() {
	
    }
    
    
    
    public Speciality(int id, String name, int minScore, int limit) {
	super();
	this.id = id;
	this.name = name;
	this.minScore = minScore;
	this.limit = limit;
    }



    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getMinBall() {
        return minScore;
    }
    public int getLimit() {
        return limit;
    }
    
    
}
