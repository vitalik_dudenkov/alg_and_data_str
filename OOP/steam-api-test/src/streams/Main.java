package streams;

//import java.util.stream.Collectors;
// ����������� ������ ����������� ��� ����������� ������ (�������������� ������� �������)
import static java.util.stream.Collectors.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.*;

public class Main {
	public static void main(String[] args) {
	    
	    List<Speciality> specialities = new ArrayList<>();
	    
	    Path file = Paths.get("directions.txt");
	    List<String> lines = new ArrayList<>();
	    try {
		lines = Files.readAllLines(file);
	    }catch (IOException e) {
		e.printStackTrace();
		return;
	    }
	    for(String line : lines) {
		String[] elements = line.split(";");
		specialities.add(new Speciality(Integer.parseInt(elements[0]), elements[1],
			Integer.parseInt(elements[2]), Integer.parseInt(elements[3])));
	    }
	    
	    List<Student> students = new ArrayList<>();
	    lines = new ArrayList<>();
	    
	    Path path = Paths.get("students.txt");
	    try {
		lines = Files.readAllLines(path);
	    }catch (IOException e) {
		e.printStackTrace();
		return;
	    }
	    for(String line : lines) {
		String[] elements = line.split(";");
		Student st = new Student(Integer.parseInt(elements[0]), elements[1],
			elements[2], Integer.parseInt(elements[3]), Integer.parseInt(elements[4]),
			Integer.parseInt(elements[5]), Integer.parseInt(elements[6]));
		st.setFullScore(st.getInfScore() + st.getMathScore() + st.getRusScore());
		students.add(st);
	    }
	    
	    firstTask(students);
	    
	    
	    
/*		// ������� ���� ��� ������, �� ������ �������� ����� ����������� �����������.
		List<Dish> menu = Arrays.asList(
			new Dish("pork", false, 800, Dish.Type.MEAT),
			new Dish("beef", false, 700, Dish.Type.MEAT),
			new Dish("chicken", false, 400, Dish.Type.MEAT),
			new Dish("french fries", true, 530, Dish.Type.OTHER),
			new Dish("rice", true, 350, Dish.Type.OTHER),
			new Dish("season fruit", true, 120, Dish.Type.OTHER),
			new Dish("pizza", true, 550, Dish.Type.OTHER),
			new Dish("prawns", false, 300, Dish.Type.FISH),
			new Dish("salmon", false, 450, Dish.Type.FISH) );
		
		// ������� �� ������ ������ ���� 3 ���������� �����
		List<String> threeHighCaloricDishName = 
				menu.stream().
				filter(p -> p.getCalories() > 300).
				map(Dish::getName).
				limit(3).
				collect(toList());
		System.out.println(threeHighCaloricDishName);
		
		// ��������� ��� ���� ���, ���� ���������� ��� �������, �������������� �������, ��������
		threeHighCaloricDishName = 
				menu.stream().
				filter(d ->{
					System.out.println("filtering " + d);
					return d.getCalories() > 300;
					}).
				map(d ->{
					System.out.println("mapping " + d);
					return d.getName();
				}).
				limit(3).
				collect(toList());
		// � ��� ������� �� �������? ������ � ���, ��� takeWhile ��������� ������
		// ��� ������ �������� ������ ������������ ������� �������
		// ������� ���� �������� ���� �����-�� ����� ��� �������� ������������� ������� ��� ������)
		List<Dish> slicedMenu =
				menu.stream().
				sorted(Comparator.comparing(Dish::getCalories)).
				takeWhile(d -> d.getCalories() < 320).
				collect(toList());
		System.out.println(slicedMenu);
		
		// dropWhile ��������� ��� ��������, ��� ������� �������� �������, � ��� ��������� ������
		// (��� ������ 1 ��� �������� ������ ������, dropWhile ��������� ������ � ������ ��� ��������� ��������)
		slicedMenu = 
				menu.stream().
				dropWhile(d -> d.getCalories() > 320).
				collect(toList());
		System.out.println(slicedMenu);
		
		//����������, ��� � ��� ���� 2 ������� ����� [1, 2, 3] � [3, 4]
		// ���������� ������� ������ �������� � �� ��������� �����������
		// �������� [(1, 2), (1, 3), (2, 2)...]
		// �������, ���� ��������� ������ �� ����, ����� ������� ������ ���
		List<Integer> numbers1 = Arrays.asList(1, 2, 3);
		List<Integer> numbers2 = Arrays.asList(3, 4);
		List<int[]> pairs = numbers1.stream()
				.flatMap(i -> numbers2.stream()
						.filter(j -> (i + j) % 3 == 0)
						.map(j -> new int[] {i, j})
						)
				.collect(toList());
		// ��� �������� ����������� ������������� �� ������ �� �������������
		// ������� ������������� ������ ������ ��������
		// � ���� ������ ����
		System.out.println(pairs.get(0)[0] + " " + pairs.get(0)[1]); // 2 4
		
		//��� ������ �������� reduce ����� ��������� "������",
		// �� ���� ���-�� ������� � ���������� ������ � � ���������� ��������, ��������, �����
		Integer sumNums = numbers1.stream().reduce(0, Integer::sum);
		System.out.println(sumNums); // 6
		
		// ��� �� ����� �� ����� �����������
		// reduce(startVal, BinaryOperator<T>) ��� ��������� �������� � �������, ���������� � T � ������������ T
		Integer mulNums = numbers1.stream().reduce(1, (a, b) -> a*b);
		System.out.println(mulNums); // 6
		
		// ����� �� ���������� ������ �������� ������� reduce(), �� ����� ������� ���������� ������� ��� ������� ������
		sumNums = numbers1.stream().reduce(Integer::sum).orElse(0);
		System.out.println(sumNums); // 6
		
		// ��������� ���������� ����
		Integer count = menu.stream().map(d -> 1).reduce(Integer::sum).orElse(0);
		Long cnt = menu.stream().count();
		System.out.println(count); // 9
		System.out.println(cnt); // 9
		
		
		// ����� ���������� �������
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario", "Milan");
		Trader alan = new Trader("Alan", "Cambridge");
		Trader brian = new Trader("Brian", "Cambridge");
		
		List<Transaction> transactions = Arrays.asList(
				new Transaction("4AhfF6", brian, 2011, 300),
				new Transaction("GdGVc5", raoul, 2012, 1000),
				new Transaction("87HHvdy", raoul, 2011, 400),
				new Transaction("LjnHk7", mario, 2012, 710),
				new Transaction("111Rddf", mario, 2012, 700),
				new Transaction("Gbcfd5f", alan, 2012, 950)
		);
		
		// 1) ����� ��� ���������� �� 2011 ��� � ������������� �� �����
		List<Transaction> for2011 = transactions.stream()
				.filter(t -> t.getYear() == 2011)
				.sorted(Comparator.comparing(Transaction::getValue))
				.collect(toList());
		System.out.println(for2011);
		
		// 2) ������� ������ ��������������� �������, � ������� �������� ��������
		List<String> cities = transactions.stream()
				.map(Transaction::getTrader)
				.map(Trader::getCity)
				.distinct()
				.collect(toList());
		System.out.println(cities);
		
		// 3) ����� ���� �������� �� ��������� � ������������� �� ������
		List<Trader> traders = transactions.stream()
				.map(Transaction::getTrader)
				.filter(t -> t.getCity().equals("Cambridge"))
				.distinct()
				.sorted(Comparator.comparing(Trader::getName))
				.collect(toList());
		System.out.println(traders);
		
		// 4) ������� ������ �� ����� ������� ���������, ���������������� � ���������� �������
		String tradNames = transactions.stream()
				.map(Transaction::getTrader)
				.distinct()
				.map(Trader::getName)
				.sorted()
				.reduce((a, b) -> a + " " + b)
				.orElse(" ");
		System.out.println(tradNames);
		
		// 5) ��������, ���������� �� ���� ���� ������� �� ������
		System.out.println(transactions.stream().map(Transaction::getTrader).anyMatch(t -> t.getCity().equals("Milan")));
		
		// 6) ������� ����� ���� ���������� ��������� �� ���������
		Integer transSum = transactions.stream()
				.filter(t -> t.getTrader().getCity().equals("Cambridge"))
				.map(Transaction::getValue)
				.reduce(Integer::sum)
				.orElse(0);
		System.out.println(transSum);
		
		// 7) ����� ������������ ����� ����� ���� ����������
		Integer maxSum = transactions.stream()
				.map(Transaction::getValue)
				.reduce(Integer::max)
				.orElse(0);
		System.out.println(maxSum);
		
		// 8) ����� ���������� � ����������� ������
		Transaction trans = transactions.stream()
				.filter(t -> t.getValue() == transactions.stream()
					.map(Transaction::getValue)
					.reduce(Integer::min).orElse(0))
				.findAny()
				.get();
		System.out.println(trans);
		
		// ������� ����� � IntStream
		
		// ������� ��� ���������� ������ (��, ��������, � ��������� �� 1 �� 100) � ������ ������ 5
		IntStream.rangeClosed(1, 100).boxed()
			.flatMap(a ->
					IntStream.rangeClosed(a, 100)
						.filter(b -> Math.sqrt(a*a + b*b)%1 == 0)
						.mapToObj(b -> 
								new int[] {a, b, (int) Math.sqrt(a*a + b*b)})
						)
			.limit(5)
			.forEach(arr -> System.out.println(arr[0] + " " + arr[1] + " " + arr[2]));
		
		// � ��� ������� ��������
		IntStream.rangeClosed(1, 100).boxed()
			.flatMap(a ->
					IntStream.rangeClosed(a, 100)
						.mapToObj(b ->
								new double[] {a, b, Math.sqrt(a*a + b*b)})
						.filter(t -> t[2]%1 == 0))
			.limit(5)
			.forEach(arr -> System.out.println(arr[0] + " " + arr[1] + " " + arr[2]));
		
				
		// ��������� ��� ���������� ��������
		// ������� ������ 10 ��������� ����
		Stream.iterate(new int[] {0, 1},
					t -> new int[] {t[1], t[0] + t[1]})
			.limit(5)
			.map(t -> t[0])
			.forEach(System.out::println);
		
		
		// ������� � ���� � �������� �¨���� collect()
		
		//��� ���� ������ ��������� ������� ����� ����
		// (�������, ������ �� ����� ������� ������ ��� �������� �������� ���������� ��������� ���������)
		// ������ ��� ��� ����� ������� �� ������� menu.size(), ������ ���� ������� ��� �������� ���������� ������� ����� ���������
		Long menuSize = menu.stream().collect(counting());
		System.out.println(menuSize);
		
		// ��������� ����� ����� ���������� �����. ������������� maxBy(Comparator<T>)
		// ���� �� ���������, ������� �������� ����� ��� ����������, � ����� ��������� ��� �����������
		Comparator<Dish> dishCaloriesComparator =
				Comparator.comparing(Dish::getCalories); // �� ������������
		Optional<Dish> mostCalorieDish =
				menu.stream()
					.collect(maxBy(dishCaloriesComparator)); // Optional �� ������ ������� ����
		System.out.println(mostCalorieDish.orElseThrow()); // ����� �� �����, ��� ���������� NoSuchElementException (��� ����� ����������)
		
		// summingInt ��������� �� ����� �������, ������������ ������ � ����������� �������� (int)
		// ��������� ������ ������������ ����
		System.out.println(menu.stream().collect(summingInt(Dish::getCalories)));
		
		//averagingInt ��������� �� �� ���������, �� �� ���������, � ��������� �������
		System.out.println(menu.stream().collect(averagingInt(Dish::getCalories)));
		
		// � ���� � ����� ���� ��� ���������� ����� �������� � ����� � ������� � ������� � ��������?
		IntSummaryStatistics menuStatistics = menu.stream().collect(summarizingInt(Dish::getCalories));
		System.out.println("�������: " + menuStatistics.getAverage() 
			+ ", �����: " + menuStatistics.getSum() + ", ����������: " + menuStatistics.getCount() 
			+ ", ��������: " + menuStatistics.getMax() + ", �������: " + menuStatistics.getMin());
		System.out.println(menuStatistics); // �� �� �����, �� ������� � ������� ��� �������
		
		// ��... � ���� ������ �� ����� ������� �� ����� ��� �������� ����
		String shortMenu = menu.stream().map(Dish::getName).collect(joining(", "));
		System.out.println(shortMenu);
		
		Map<Dish.Type, Integer> totalCaloriesByType =
				menu.stream()
				.collect(groupingBy(Dish::getType,
						summingInt(Dish::getCalories)));
		System.out.println(totalCaloriesByType);
		
		Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType =
				menu.stream().collect(
						groupingBy(Dish::getType, mapping(dish -> {
						if (dish.getCalories() <= 400) return CaloricLevel.DIET;
						else if(dish.getCalories() <= 700) return CaloricLevel.NORMAL;
						else return CaloricLevel.FAT;
						}, toSet())));
		System.out.println(caloricLevelsByType);
		
		//��������� ������ ��������������� ����������� ����������
		List<Dish> dishes = menu.stream().collect(new ToListCollector<>());
		System.out.println(dishes);
		
		// � ���� �� ������������ ���� ���������, �� ����� � ���
		// ������������� ����� collect ��������� ��� �������:
		// supplier, accumulator, combiner (�� ��, ��� ����� ���� ��� ������������ ����������)
		dishes = menu.stream().collect(
					ArrayList::new,
					List::add,
					List::addAll); */
		
		
		
	}
	
	//� ����� ����������� ���� � ���������, ������� ������ ����������
	public static void firstTask(List<Student> lst) {
	    List<Student> res = lst.stream().
		    sorted(Comparator.comparing(Student::getLastName)).
		    collect(Collectors.toList());
	    System.out.println(res);
	}
	
	public static void secondTask(List<Student> students, List<Speciality> specialities) {
	    List<Student> res = students.stream()
		    .filter(t -> (t.getInfScore() + t.getMathScore() + t.getRusScore()) >= (specialities.stream()
			    							.map(Speciality::getMinBall)
			    							.min(Integer::compareTo)
			    							.get()))
		    .sorted(Comparator.comparing(Student::getFullScore).reversed())
		    .collect(Collectors.toList());
	    System.out.println(res);
	}
	
	public static void thirdTask(List<Student> students, List<Speciality> specialities, String direction) {
	    List<Student> res = students.stream()
		    .filter(t -> t.getDirectId() == specialities.stream()
				.filter(d -> d.getName().equals(direction))
				.map(Speciality::getId)
				.findFirst()
				.get())
		    .sorted(Comparator.comparing(Student::getFullScore).reversed())
		    .limit(3)
		    .collect(Collectors.toList());
	    System.out.println(res);  
	}
	
	public static void fourthTask(List<Student> students, List<Speciality> specialities, String direction) {
	    List<Student> res = students.stream()
		    .filter(t -> (t.getDirectId() == specialities.stream()
				.filter(d -> d.getName().equals(direction))
				.map(Speciality::getId)
				.findFirst()
				.get()) && (t.getFullScore() >= specialities.stream()
								.filter(d -> d.getId() == t.getDirectId())
								.findFirst()
								.get().getMinBall()))
		    .sorted(Comparator.comparing(Student::getFullScore).reversed().thenComparing(Student::getLastName))
		    .collect(Collectors.toList());
	    System.out.println(res);
	}
	
	public static void fifthTask(List<Student> students, List<Speciality> specialities, String direction) {
	    List<Student> res = students.stream()
		    .filter(t -> t.getDirectId() == specialities.stream()
				.filter(d -> d.getName().equals(direction))
				.map(Speciality::getId)
				.findFirst()
				.get())
		    .sorted(Comparator.comparing(Student::getFullScore).reversed())
		    .limit(specialities.stream().
			    filter(d -> d.getName().equals(direction))
			    .findFirst()
			    .get()
			    .getLimit())
		    .sorted(Comparator.comparing(Student::getFullScore).reversed().thenComparing(Student::getLastName))
		    .collect(Collectors.toList());
	    System.out.println(res);
	}
	
	public static void sixthTask(List<Student> students, List<Speciality> specialities, String direction) {
	    List<Student> res = students.stream()
		    .filter(t -> t.getDirectId() == specialities.stream()
				.filter(d -> d.getName().equals(direction))
				.map(Speciality::getId)
				.findFirst()
				.get())
		    .sorted(Comparator.comparing(Student::getFullScore).reversed())
		    .skip((long) specialities.stream().
			    filter(d -> d.getName().equals(direction))
			    .findFirst()
			    .get()
			    .getLimit())
		    .collect(Collectors.toList());
	    System.out.println(res.size() > 0 ? res.get(0) : "����� ���");
	}
	
	public static void seventhTask(List<Student> students, List<Speciality> specialities) {
	    specialities.stream()
	    .forEach(el -> System.out.println(el + "------" + (students.stream()
		    	.filter(t -> t.getDirectId() == specialities.stream()
		    		.filter(d -> d.getName().equals(el))
		    		.map(Speciality::getId)
		    		.findFirst()
		    		.get())
	    		.count())));
	}
	
	public static void eighthTask(List<Student> students, List<Speciality> specialities) {
	    double res = students.stream()
		    .filter(t -> t.getDirectId() == specialities.stream()
				.filter(d -> d.getName().equals(""))
				.map(Speciality::getId)
				.findFirst()
				.get())
		    .sorted(Comparator.comparing(Student::getFullScore).reversed())
		    .limit(specialities.stream().
			    filter(d -> d.getName().equals(""))
			    .findFirst()
			    .get()
			    .getLimit())
		    .collect(averagingInt(Student::getFullScore));
	}
}
