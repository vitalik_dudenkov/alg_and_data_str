﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JumpingFrogs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_DragDrop(object sender, DragEventArgs e)
        {
            // картинки для игроков натаскиваются (не хранятся в ресурсах)
            string[] nameFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
            int index = 0;
            foreach(PictureBox pb in groupBox1.Controls)
            {
                pb.Load(nameFiles[index++]);
                pb.SizeMode = PictureBoxSizeMode.AutoSize;
            }
        }

        private void panel1_DragEnter(object sender, DragEventArgs e)
        {
            if(e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // Если пользователь отменил свой выбор - удалить картинки
            foreach (PictureBox pb in groupBox1.Controls)
            {
                pb.Image = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // передаю между формами информацию о картинках через конструктор (по принципам ООП
            Form f = new Form2(this.pictureBox1.Image, this.pictureBox2.Image);
            f.Show();
        }
    }
}
