﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JumpingFrogs
{
    public partial class Form2 : Form
    {

        private PictureBox pbSource;

        List<PictureBox> listPb;
        List<string> names;
        public Form2(Image firstPlayer, Image secondPlayer)
        {
            InitializeComponent();
            //            int index = groupBox1.Controls.Count / 2 + 1;

            // потом это будет заменено на цикл (для динамического выбора количества лягух в команде)
            this.pictureBox1.Image = firstPlayer;
            this.pictureBox2.Image = firstPlayer;
            this.pictureBox3.Image = firstPlayer;
            this.pictureBox4.Image = Properties.Resources.stone;
            this.pictureBox5.Image = secondPlayer;
            this.pictureBox6.Image = secondPlayer;
            this.pictureBox7.Image = secondPlayer;

            listPb = new List<PictureBox>();
            foreach(PictureBox pb in groupBox1.Controls)
            {
                listPb.Add(pb);
            }

            //Пока захардкодим позиции элементов в спике
            //В последствии они будут заполняться вместе с картинками
            names = new List<string>();
            names.Add("X");
            names.Add("X");
            names.Add("X");
            names.Add("_");
            names.Add("0");
            names.Add("0");
            names.Add("0");

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            foreach(PictureBox pb in listPb)
            {
                pb.AllowDrop = true;
                pb.MouseDown += new MouseEventHandler(pb_MouseDown);
                pb.DragDrop += new DragEventHandler(pb_DragDrop);
                pb.DragEnter += new DragEventHandler(pb_DragEnter);
            }
        }

        private void pb_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void pb_DragDrop(object sender, DragEventArgs e)
        {
            // получаем ссылку на картинку С КОТОРОЙ мы будем меняться
            PictureBox pbGlobal = (PictureBox)sender;
            
            // камешек пустой мы ни с чем менять не будем
            if (listPb.IndexOf(pbSource) != names.IndexOf("_"))
            {
                //находим позиции на поле обеих карточек, что хотим менять местами
                int idxSource = listPb.IndexOf(pbSource);
                int idxGlobal = listPb.IndexOf(pbGlobal);
                // проверяем, что мы не пытаемся менять местами лягух и что делаем шаг не более чем на 2
                // здесь не проверяется то, что левые могут ходить только направо (и наоборот)
                if(names[idxGlobal] == "_"
                    && Math.Abs(idxSource - idxGlobal) < 3)
                {
                    // меняем местами картинки
                    (pbGlobal.Image, pbSource.Image) =
                        ((Bitmap)e.Data.GetData(DataFormats.Bitmap), (Bitmap)pbGlobal.Image);
                    // а вместе с ними и текущее состояние игрового поля
                    var buf = names[idxGlobal];
                    names[idxGlobal] = names[idxSource];
                    names[idxSource] = buf;
                }
            }
        }

        private void pb_MouseDown(object sender, MouseEventArgs e)
        {
            // тут ссылка на картинку, котороую мы будем пытаться двигать
            pbSource = (PictureBox)sender;
            pbSource.DoDragDrop(pbSource.Image, DragDropEffects.Copy);
        }
    }
}
